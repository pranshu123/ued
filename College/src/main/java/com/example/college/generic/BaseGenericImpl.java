package com.example.college.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

@SuppressWarnings({ "unchecked", "deprecation" })
public class BaseGenericImpl<E> implements BaseGeneric<E> {

	Session session;

	@PersistenceContext
	public EntityManager entityManager;
	private final Class<E> entityClass;

	public BaseGenericImpl() {
		this.entityClass = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	public Session getSession() {
		session = this.entityManager.unwrap(Session.class);
		if (session.getTransaction().isActive()) {
			return session;
		}
		session.beginTransaction();
		return session;
	}

	@Override
	public E getById(Class<E> type, Serializable id) {
		session = getSession();
		return session.get(this.entityClass, id);
	}

	@Override
	public boolean deleteById(Serializable id) {
		session = getSession();
		Object persistent = session.load(this.entityClass, id);

		try {
			session.delete(persistent);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public void saveOrUpdate(E entity) {
		session = getSession();
		session.saveOrUpdate(entity);
	}

	@Override
	public List<E> findAll() {
		session = getSession();
		List<E> list = session.createCriteria(this.entityClass).list();
		return list;
	}
}
