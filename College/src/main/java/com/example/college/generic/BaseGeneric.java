package com.example.college.generic;

import java.io.Serializable;
import java.util.List;

public interface BaseGeneric<E> {
	List<E> findAll();
	E getById(Class<E> type, Serializable id);
	boolean deleteById(Serializable id);
	public void saveOrUpdate(E entity);

}
