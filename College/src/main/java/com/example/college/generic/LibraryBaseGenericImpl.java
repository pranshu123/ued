package com.example.college.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

@SuppressWarnings({ "unchecked", "deprecation" })
public class LibraryBaseGenericImpl<B> implements LibraryBaseGeneric<B> {

	Session session;

	@PersistenceContext
	public EntityManager entityManager;
	private final Class<B> entityClass;

	public LibraryBaseGenericImpl() {
		this.entityClass = (Class<B>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];

	}

	public Session getSession() {
		session = this.entityManager.unwrap(Session.class);
		if (session.getTransaction().isActive()) {
			return session;
		}
		session.beginTransaction();
		return session;

	}

	/* get data by id */
	@Override
	public B getById(Class<B> type, Serializable id) {
		session = getSession();
		return session.get(this.entityClass, id);
	}

	/* deleting data by id */
	@Override
	public boolean deleteById(Serializable id) {
		session = getSession();
		Object persistent = session.load(this.entityClass, id);

		try {
			session.delete(persistent);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/* save and update */
	public void saveOrUpdate(B entity) {
		session = getSession();
		session.saveOrUpdate(entity);

	}

	/* save and update */
	public Serializable save(Object object) {
		session = getSession();
		session.save(object);
		return (Serializable) object;

	}

	/* getting all data */
	public List<B> findAll() {
		session = getSession();
		List<B> list = session.createCriteria(this.entityClass).list();
		return list;

	}

	/* get data using student id */

	@Override
	public B getByStudentId(Class<B> type, Serializable id) {
		session = getSession();
		return session.get(this.entityClass, id);
	}
}
