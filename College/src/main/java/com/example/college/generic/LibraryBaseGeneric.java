package com.example.college.generic;

import java.io.Serializable;
import java.util.List;

public interface LibraryBaseGeneric<B> {
	List<B> findAll();

	B getById(Class<B> type, Serializable id);

	boolean deleteById(Serializable id);

	public void saveOrUpdate(B entity);

	B getByStudentId(Class<B> type, Serializable id);

}
