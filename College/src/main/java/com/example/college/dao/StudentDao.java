package com.example.college.dao;

import com.example.college.generic.BaseGeneric;
import com.example.college.model.StudentModel;

public interface StudentDao extends BaseGeneric<StudentModel> {

}
