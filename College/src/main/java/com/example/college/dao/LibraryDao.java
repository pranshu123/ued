package com.example.college.dao;

import com.example.college.generic.LibraryBaseGeneric;
import com.example.college.model.LibraryModel;

public interface LibraryDao extends LibraryBaseGeneric<LibraryModel> {

}
