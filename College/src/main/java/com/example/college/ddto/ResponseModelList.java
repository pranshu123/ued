package com.example.college.ddto;

import java.util.List;

public class ResponseModelList<T> {
	private boolean status;
	private String message;
	private List<T> object;
	private Integer noOfPage[];

	public ResponseModelList(boolean status, String message, List<T> object) {
		this.status = status;
		this.message = message;
		this.object = object;
	}

	public ResponseModelList(boolean status, String message, List<T> object, Integer[] noOfPage) {
		super();
		this.status = status;
		this.message = message;
		this.object = object;
		this.noOfPage = noOfPage;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<T> getObject() {
		return object;
	}

	public void setObject(List<T> object) {
		this.object = object;
	}

	public Integer[] getNoOfPage() {
		return noOfPage;
	}

	public void setNoOfPage(Integer[] noOfPage) {
		this.noOfPage = noOfPage;
	}

}
