package com.example.college.daoImpl;

import org.springframework.stereotype.Repository;

import com.example.college.dao.LibraryDao;
import com.example.college.generic.LibraryBaseGenericImpl;
import com.example.college.model.LibraryModel;

@Repository
public class LibraryDaoImpl extends LibraryBaseGenericImpl<LibraryModel> implements LibraryDao {

}
