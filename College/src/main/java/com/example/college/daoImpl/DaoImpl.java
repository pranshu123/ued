package com.example.college.daoImpl;

import org.springframework.stereotype.Repository;

import com.example.college.dao.StudentDao;
import com.example.college.generic.BaseGenericImpl;
import com.example.college.model.StudentModel;

@Repository
public class DaoImpl extends BaseGenericImpl<StudentModel> implements StudentDao {

}
