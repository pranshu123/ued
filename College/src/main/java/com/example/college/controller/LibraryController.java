package com.example.college.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.college.model.LibraryModelDto;
import com.example.college.service.LibraryService;

@RestController
@RequestMapping("library")
public class LibraryController {

	@Autowired
	LibraryService service;

	@PostMapping("/create")
	public LibraryModelDto saveOrUpdate(@RequestBody LibraryModelDto libraryModelDto) {
		return service.saveOrUpdate(libraryModelDto);
	}

	@GetMapping("getById")
	public LibraryModelDto getById(@RequestHeader(value = "bookid", defaultValue = "") String bookid) {
		return service.getById(bookid);
	}

	@GetMapping(value = "getAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LibraryModelDto> getAll() {
		return service.getAll();
	}

	@DeleteMapping("deleteById")
	public boolean deleteById(@RequestHeader(value = "bookid", defaultValue = "") String bookid) {
		return service.deleteById(bookid);
	}

	@GetMapping("getByStudentId")
	public LibraryModelDto getByStudentId(@RequestHeader(value = "studentid", defaultValue = "") String studentid) {
		return service.getByStudentId(studentid);

	}

}
