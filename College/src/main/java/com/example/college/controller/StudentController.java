package com.example.college.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.college.model.StudentModelDto;
import com.example.college.service.StudentService;

@RestController
@RequestMapping("student")
public class StudentController {
	@Autowired
	StudentService service;

//	@PostMapping("/create1")
//	public List<StudentModelDto> create(
//			@RequestHeader(value="studentName", defaultValue="")String studentName,
//			@RequestHeader(value="studentDepartment", defaultValue="")String studentDepartment,
//			@RequestHeader(value="studentYear", defaultValue="")int studentYear){
//		       
//		    List<StudentModelDto> list = new List<>();
//		    return list;
//		
//	}
	@PostMapping("/create")
	public StudentModelDto saveorUpdate(@RequestBody StudentModelDto studentModelDto) {
		return service.saveOrUpdate(studentModelDto);
	}

	@GetMapping("getById")
	public StudentModelDto getById(@RequestHeader(defaultValue = "", value = "studentid") String studentid) {
		return service.getById(studentid);
	}

	@GetMapping(value = "getAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<StudentModelDto> getAll() {
		return service.getAll();

	}

	@DeleteMapping("deleteById")
	public boolean deleteById(@RequestHeader(value = "studentid", defaultValue = "") String studentid) {
		return service.deleteById(studentid);
	}

}
