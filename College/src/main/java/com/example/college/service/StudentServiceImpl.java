package com.example.college.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.college.daoImpl.DaoImpl;
import com.example.college.mapper.StudentMapper;
import com.example.college.model.StudentModel;
import com.example.college.model.StudentModelDto;

@Service
@Transactional(readOnly = true)
public class StudentServiceImpl implements StudentService {

	@Autowired
	DaoImpl studentdaoimpl;

	@Override
	@Transactional(readOnly = false)
	public StudentModelDto saveOrUpdate(StudentModelDto studentModelDto) {
		Date date = new Date();
		long id = date.getTime();
		String studentId = "" + id;

		StudentModel studentModel = new StudentModel();

		if (studentModelDto.getStudentId() == null) {
			studentModelDto.setStudentId(studentId);
			studentModel = StudentMapper.INSTANCE.dtoToModel(studentModelDto);
//			studentModel.getLibraryModels().add(libraryModel);

		} else {

		}
		studentdaoimpl.saveOrUpdate(studentModel);
		return studentModelDto;
	}

	@Override
	public StudentModelDto getById(String studentid) {
		int id = Integer.parseInt(studentid);
		StudentModel studentModel = (StudentModel) studentdaoimpl.getById(StudentModel.class, id);

		StudentModelDto studentModelDto = StudentMapper.INSTANCE.modelToDto(studentModel);
		return studentModelDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<StudentModelDto> getAll() {
		List<StudentModel> stuModel = new ArrayList<>();
		List<StudentModelDto> stuDtos = new ArrayList<>();
		stuModel = studentdaoimpl.findAll();
		if (stuModel != null) {
			for (StudentModel studentModel : stuModel) {
				StudentModelDto dto = StudentMapper.INSTANCE.modelToDto(studentModel);

				stuDtos.add(dto);
			}
		}
		return stuDtos;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deleteById(String studentid) {

		studentdaoimpl.deleteById(studentid);
		return true;
	}

}
