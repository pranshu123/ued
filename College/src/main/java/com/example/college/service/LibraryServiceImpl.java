package com.example.college.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.college.daoImpl.LibraryDaoImpl;
import com.example.college.mapper.StudentMapper;
import com.example.college.model.LibraryModel;
import com.example.college.model.LibraryModelDto;
import com.example.college.model.StudentModel;

@Service
@Transactional(readOnly = true)
public class LibraryServiceImpl implements LibraryService {

	@Autowired
	LibraryDaoImpl libraryDaoImpl;

	@Override
	@Transactional(readOnly = false)
	public LibraryModelDto saveOrUpdate(LibraryModelDto libraryModelDto) {
		Date date = new Date();
		long id = date.getTime();
		try {
			String bookId = "" + id;

			LibraryModel libraryModel = new LibraryModel();
			if (libraryModelDto.getBookId() == null) {
				libraryModelDto.setBookId(bookId);
				libraryModel.setBookId(libraryModelDto.getBookId());
				libraryModel.setBookName(libraryModelDto.getBookName());
				StudentModel studentModel = StudentMapper.INSTANCE.dtoToModel(libraryModelDto.getStudentModelDto());
				libraryModel.setStudentModel(studentModel);
//				// studentModelDto.libraryModelDto.setBookId(bookId);

			} else {
				System.out.println("");
			}

			libraryDaoImpl.saveOrUpdate(libraryModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return libraryModelDto;
	}

	@Override
	public LibraryModelDto getById(String bookid) {

		LibraryModel libraryModel = (LibraryModel) libraryDaoImpl.getById(LibraryModel.class, bookid);
		LibraryModelDto libraryModelDto = new LibraryModelDto(bookid, bookid, null);
		libraryModelDto.setBookId(libraryModel.getBookId());
		libraryModelDto.setBookName(libraryModel.getBookName());

		return libraryModelDto;
	}

	@Override
	public List<LibraryModelDto> getAll() {
		List<LibraryModel> libModel = new ArrayList<>();
		List<LibraryModelDto> libDtos = new ArrayList<>();
		libModel = libraryDaoImpl.findAll();

		if (libModel != null) {
			for (LibraryModel libraryModel : libModel) {
				LibraryModelDto dto = new LibraryModelDto(null, null, null);

				dto.setBookId(libraryModel.getBookId());
				dto.setBookName(libraryModel.getBookName());
				libDtos.add(dto);
			}
		}

		return libDtos;

	}

	@Override
	@Transactional(readOnly = false)
	public boolean deleteById(String bookid) {

		libraryDaoImpl.deleteById(bookid);
		return true;

	}

	@Override
	public LibraryModelDto getByStudentId(String studentid) {
		// TODO Auto-generated method stub
		return null;
	}

}
