package com.example.college.service;

import java.util.List;

import com.example.college.model.LibraryModelDto;

public interface LibraryService {
	public LibraryModelDto saveOrUpdate(LibraryModelDto libraryModelDto);

	public LibraryModelDto getById(String bookid);

	public List<LibraryModelDto> getAll();

	public boolean deleteById(String bookid);

	public LibraryModelDto getByStudentId(String studentid);

}
