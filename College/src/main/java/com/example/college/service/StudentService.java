package com.example.college.service;

import java.util.List;

import com.example.college.model.StudentModelDto;

public interface StudentService {
	public StudentModelDto saveOrUpdate(StudentModelDto studentModelDto);
	public StudentModelDto getById(String studentid);
	public List<StudentModelDto> getAll();
	public boolean deleteById(String studentid);

}
