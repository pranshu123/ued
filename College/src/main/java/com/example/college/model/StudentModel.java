package com.example.college.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "student")
public class StudentModel implements Serializable {

	private List<LibraryModel> libraryModels = new ArrayList<>();

	private String studentId;
	private String studentName;
	private String studentDepartment;
	private int studentYear;

	@Id
	@Column(name = "studentId", nullable = false, length = 40, unique = true)
	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	@Column(name = "studentName")
	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	@Column(name = "studentDepartment")
	public String getStudentDepartment() {
		return studentDepartment;
	}

	public void setStudentDepartment(String studentDepartment) {
		this.studentDepartment = studentDepartment;
	}

	@Column(name = "studentYear")
	public int getStudentYear() {
		return studentYear;
	}

	public void setStudentYear(int studentYear) {
		this.studentYear = studentYear;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentModel")
	public List<LibraryModel> getLibraryModels() {
		return libraryModels;
	}

	public void setLibraryModels(List<LibraryModel> libraryModels) {
		this.libraryModels = libraryModels;
	}

}
