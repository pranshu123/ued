package com.example.college.model;

import java.util.List;

public class StudentModelDto {
	public String studentId;
	public String studentName;
	public String studentDepartment;
	public int studentYear;

	public List<LibraryModelDto> libraryModelDto;

	public List<LibraryModelDto> getLibraryModelDto() {
		return libraryModelDto;
	}

	public void setLibraryModelDto(List<LibraryModelDto> libraryModelDto) {
		this.libraryModelDto = libraryModelDto;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentDepartment() {
		return studentDepartment;
	}

	public void setStudentDepartment(String studentDepartment) {
		this.studentDepartment = studentDepartment;
	}

	public int getStudentYear() {
		return studentYear;
	}

	public void setStudentYear(int studentYear) {
		this.studentYear = studentYear;
	}

	public StudentModelDto() {
		super();
	}

}
