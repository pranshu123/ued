package com.example.college.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "library")
public class LibraryModel implements Serializable {

	private static final long serialVersionUID = -6283465622205002131L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "studentId")
	private StudentModel studentModel;

	@Id
	@Column(name = "book_id", length = 40, nullable = false, unique = true)
	private String bookId;

	@Column(name = "book_name", length = 40, nullable = false)
	private String bookName;

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public LibraryModel(String bookId, String bookName) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
	}

	public LibraryModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentModel getStudentModel() {
		return studentModel;
	}

	public void setStudentModel(StudentModel studentModel) {
		this.studentModel = studentModel;
	}

}
