package com.example.college.model;

import java.io.Serializable;

public class LibraryModelDto {
	private String bookId;
	private String bookName;
	public StudentModelDto studentModelDto;

	public StudentModelDto getStudentModelDto() {
		return studentModelDto;
	}

	public void setStudentModelDto(StudentModelDto studentModelDto) {
		this.studentModelDto = studentModelDto;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public LibraryModelDto(String bookId, String bookName, StudentModelDto studentModelDto) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.studentModelDto = studentModelDto;
	}

	public LibraryModelDto() {
		super();
	}

	public LibraryModelDto(Serializable bookid2, Serializable bookid3, Object studentModelDto2) {
		super();
	}

}
