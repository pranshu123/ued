package com.example.college.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.example.college.model.StudentModel;
import com.example.college.model.StudentModelDto;

@Mapper
public interface StudentMapper {
	StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

	StudentModelDto modelToDto(StudentModel model);

	StudentModel dtoToModel(StudentModelDto dto);

}
