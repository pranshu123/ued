package com.ued.college.helper;

public class ConstantExtension {

	/* common messages */

	public static final String ERROR = "internal server error";
	public static final String SUCCESS = "successful";
	public static final String ALREADY_EXIST = "Already exist";
	public static final String NOT_MATCH = "Not Matched";
	public static final String INVALID = "invalid";

	/* Student Constants */

	public static final String STUD_ADDED = "student successfully added";
	public static final String STUD_UPDATED = "student successfully updated";
	public static final String STUD_DELETED = "student successfully deleted";
	public static final String STUD_SUCCESS_RECIEVED = "student successfully fetched";
	public static final String STUD_NAME_ALREADY_EXIST = "name already exist";

	/* Library Constants */

	public static final String LIB_ADDED = "book successfully added";
	public static final String LIB_UPDATED = "book successfully updated";
	public static final String LIB_DELETED = "book successfully deleted";
	public static final String LIB_SUCCESS_RECIEVED = "book successfully fetched";
	public static final String LIB_NAME_ALREADY_EXIST = "name already exist";

}
