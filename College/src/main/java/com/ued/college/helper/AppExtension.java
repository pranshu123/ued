package com.ued.college.helper;

import java.net.URLDecoder;
import java.nio.file.FileSystems;

public class AppExtension {

	public String getFolder() {
		String baseUrl = "";

		try {
			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");

			if (fullPath.contains("WEB-INF")) {
				String PathArr[] = fullPath.split("/WEB-INF/classes/");
				fullPath = PathArr[0];
			} else {
				baseUrl = FileSystems.getDefault().getPath("src", "main", "webapp").toUri().toURL().toString();
				baseUrl = baseUrl.replaceAll("file:/", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseUrl;
	}
}
