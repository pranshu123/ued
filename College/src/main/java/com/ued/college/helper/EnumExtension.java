package com.ued.college.helper;

public class EnumExtension {

	public static enum eOrderBy {
		enAsc("Asc", "Ascending Order"), enDesc("Desc", "Descending Order");

		private String firstValue;
		private String secondValue;

		eOrderBy(String firstValue, String secondValue) {
			this.firstValue = firstValue;
			this.secondValue = secondValue;
		}

		public String getKey() {
			return this.firstValue;

		}

		public String getValue() {
			return this.secondValue;
		}

		public static String getFirstValue(String value) {
			for (eOrderBy item : eOrderBy.values()) {
				if (item.getKey().equalsIgnoreCase(value)) {
					return item.getValue();
				}

			}
			return "";
		}

		public static String getSecondValue(String value) {
			for (eOrderBy item : eOrderBy.values()) {
				if (item.getKey().equalsIgnoreCase(value)) {
					return item.getValue();
				}
			}

			return "";
		}

	}

}
